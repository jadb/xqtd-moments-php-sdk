<?php
namespace Moments;

use GuzzleHttp\Psr7\Request;
use Moments\Exception\BadMethodCallException;
use Moments\Exception\UndefinedApiCallException;
use Moments\HttpClient\GuzzleClient;
use Moments\HttpClient\HttpClientInterface;
use Moments\HttpClient\Message\ResponseMediator;
use Psr\Http\Message\RequestInterface;

/**
 * Class Client
 *
 * @package Moments
 */
class Client
{
    /**
     * @var \Moments\HttpClient\HttpClientInterface
     */
    private $httpClient;

    /**
     * @param \Moments\HttpClient\HttpClientInterface|null $httpClient
     */
    public function __construct(HttpClientInterface $httpClient = null)
    {
        if ($httpClient === null) {
            $httpClient = new GuzzleClient();
        }

        $this->httpClient = $httpClient;
    }

    /**
     * @param $name
     * @return \Moments\Api\ApiInterface
     * @throws \Moments\Exception\UndefinedResourceCallException
     */
    public function api($name)
    {
        $class = 'Moments\\Api\\' . ucfirst(strtolower($name));
        if (!class_exists($class)) {
            throw new UndefinedApiCallException($name);
        }

        return new $class($this);
    }

    /**
     * @param $name
     * @param $args
     * @return \Moments\Api\ApiInterface
     * @throws \Moments\Exception\BadMethodCallException
     */
    public function __call($name, $args)
    {
        try {
            return $this->api($name);
        } catch (UndefinedApiCallException $e) {
            throw new BadMethodCallException($name);
        }
    }

    /**
     *
     * @param string $path
     * @param array $parameters
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function get($path, array $parameters = [], array $headers = [])
    {
        $request = new Request(__FUNCTION__, $path, $headers);
        return $this->mediate($request, compact('headers') + ['query' => $parameters]);
    }

    /**
     * @param string $path
     * @param array $parameters
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function post($path, array $parameters = [], array $headers = [])
    {
        $request = new Request(__FUNCTION__, $path, $headers);
        return $this->mediate($request, compact('headers') + ['json' => $parameters]);
    }

    /**
     * @param string $path
     * @param array $parameters
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function multipartPost($path, array $parameters = [], array $headers = [])
    {
        $request = new Request('post', $path, $headers);
        $multipart = $this->toMultipartParams($parameters);
        $options = compact('headers', 'multipart');
        return $this->mediate($request, $options);
    }

    /**
     * @param string $path
     * @param array $parameters
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function put($path, array $parameters = [], array $headers = [])
    {
        $request = new Request(__FUNCTION__, $path, $headers);
        return $this->mediate($request, compact('headers') + ['json' => $parameters]);
    }

    /**
     * @param string $path
     * @param array $parameters
     * @param array $headers
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function delete($path, array $parameters = [], array $headers = [])
    {
        $request = new Request(__FUNCTION__, $path, $headers);
        return $this->mediate($request, compact('headers') + ['json' => $parameters]);
    }

    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @param array $options
     * @return \Moments\HttpClient\Message\ResponseMediator
     */
    protected function mediate(RequestInterface $request, array $options = [])
    {
        return new ResponseMediator($this->httpClient->send($request, $options));
    }

    /**
     * Formats the array in a way that is expected by the Guzzle client.
     *
     * @param array $parameters
     * @return array
     * @see http://docs.guzzlephp.org/en/latest/quickstart.html#sending-form-files
     */
    protected function toMultipartParams(array $parameters)
    {
        $multipart = [];

        foreach ($parameters as $name => $contents) {
            if ($name !== 'tubes') {
                if (!is_array($contents)) {
                    $multipart[] = compact('name') + ['contents' => (string)$contents];
                    continue;
                }

                foreach ($contents as $key => $value) {
                    $multipart[] = [
                        'name' => sprintf('%s[%s]', $name, $key),
                        'contents' => (string)$value,
                    ];
                }
                continue;
            }

            foreach ($contents['tmp_name'] as $k => $filename) {
                $multipart[] = [
                    'name' => "tubes[$k]",
                    'contents' => $this->getFileContents($filename),
                ];
            }
        }

        return $multipart;
    }

    protected function getFileContents($filename)
    {
        return fopen($filename, 'r');
    }
}
