<?php
namespace Moments\Api;

use Moments\Client;

/**
 * Abstract API class.
 *
 * @package Moments
 */
abstract class AbstractApi implements ApiInterface
{

    /**
     * @var \Moments\Client
     */
    protected $client;

    /**
     * AbstractResource constructor.
     *
     * @param \Moments\Client $client
     * @param null $parent
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
