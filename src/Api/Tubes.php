<?php
namespace Moments\Api;

class Tubes extends AbstractApi
{
    const PATH = 'tubes';

    /**
     * @param string $id
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \InvalidArgumentException
     */
    public function get($id)
    {
        return $this->client->get(self::PATH . '/' . $id);
    }

    /**
     * @param string $id
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \InvalidArgumentException
     */
    public function delete($id)
    {
        return $this->client->delete(self::PATH . '/' . $id);
    }
}
