<?php
namespace Moments\Api;

use Moments\Exception\InvalidArgumentException;

class Markers extends AbstractApi
{
    const PATH = 'markers';

    /**
     * @param array|string $query
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \Moments\Exception\InvalidArgumentException
     * @throws \InvalidArgumentException
     */
    public function get($query = [])
    {
        if (!is_array($query)) {

            if (!is_string($query)) {
                throw new InvalidArgumentException();
            }

            return $this->client->get(self::PATH . '/' . $query);
        }

        $parameters = array_merge([
            'from' => null,
            'to' => null,
            'lat' => null,
            'lng' => null,
            'radius' => null,
            'unit' => null,
            'page' => 1,
            'limit' => 10,
            'direction' => 'asc',
        ], $query);

        return $this->client->get(self::PATH, array_filter($parameters));
    }

    /**
     * @param array $data
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \InvalidArgumentException
     */
    public function post(array $data)
    {
        return $this->client->multipartPost(self::PATH, $data);
    }

    /**
     * @param string $id
     * @param array $data
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \InvalidArgumentException
     */
    public function put($id, array $data)
    {
        return $this->client->multipartPost(self::PATH . '/' . $id, $data);
    }

    /**
     * @param string $id
     * @return \Moments\HttpClient\Message\ResponseMediator
     * @throws \InvalidArgumentException
     */
    public function delete($id)
    {
        return $this->client->delete(self::PATH . '/' . $id);
    }
}
