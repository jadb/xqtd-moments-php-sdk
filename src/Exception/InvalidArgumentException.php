<?php
namespace Moments\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Moments
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{

}
