<?php
namespace Moments\Exception;

use Exception;

/**
 * Class BadMethodCallException
 *
 * @package Moments
 */
class BadMethodCallException extends \BadMethodCallException implements ExceptionInterface
{
    /**
     * BadMethodCallException constructor.
     *
     * @param string $message Name of the undefined method.
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message, $code = null, Exception $previous = null)
    {
        parent::__construct(sprintf('Undefined method called: "%s"', $message), $code, $previous);
    }
}
