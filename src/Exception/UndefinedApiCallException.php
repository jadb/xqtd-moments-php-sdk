<?php
namespace Moments\Exception;

use Exception;

/**
 * Class UndefinedResourceCallException
 *
 * @package Moments
 */
class UndefinedApiCallException extends InvalidArgumentException
{
    /**
     * UndefinedResourceCallException constructor.
     *
     * @param string $message Name of the requested resource.
     * @param int $code
     * @param \Exception $previous
     */
    public function __construct($message, $code = null, Exception $previous = null)
    {
        parent::__construct(sprintf('Undefined API requested: "%s"', $message), $code, $previous);
    }
}
