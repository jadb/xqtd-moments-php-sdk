<?php
namespace Moments\HttpClient;

use GuzzleHttp\Client;
use Moments\HttpClient\Message\ResponseMediator;
use Psr\Http\Message\RequestInterface;

/**
 * GuzzleClient: bridge to use the Guzzle Client.
 *
 * Use as example to implement a bridge to your your preferred HTTP client library.
 *
 * @package Moments
 */
class GuzzleClient extends Client implements HttpClientInterface
{
    /**
     * GuzzleClient constructor.
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config + [
                // TODO: update URL to production one by default
                'base_uri' => 'http://gis.local.moments.xqtd.co',
                'headers' => [
                    'Accept' => 'application/json',
                    'User-Agent' => 'moments-php-sdk (https://github.com/xqtd/moments',
                ]
            ]);
    }
}
