<?php
namespace Moments\HttpClient;

use Psr\Http\Message\RequestInterface;

/**
 * Interface HttpClientInterface
 *
 * @package Moments\HttpClient
 */
interface HttpClientInterface
{
    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function send(RequestInterface $request, array $options = []);
}
