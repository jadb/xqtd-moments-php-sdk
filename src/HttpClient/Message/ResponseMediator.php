<?php
namespace Moments\HttpClient\Message;

use Psr\Http\Message\ResponseInterface;

/**
 * Response mediator.
 *
 * @package Moments
 */
class ResponseMediator
{
    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    private $response;

    /**
     * ResponseMediator constructor.
     *
     * @param \Psr\Http\Message\ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return array|string
     * @throws \RuntimeException
     */
    public function getContent()
    {
        $body = $this->response->getBody()->getContents();
        $content = json_decode($body, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            return $body;
        }

        return $content;
    }

    /**
     * @return array|string
     * @throws \RuntimeException
     */
    public function getData()
    {
        $content = $this->getContent();

        if (is_array($content) && array_key_exists('data', $content)) {
            return $content['data'];
        }

        return $content;
    }
}
