<?php
namespace Moments\Test\HttpClient;

use Moments\HttpClient\GuzzleClient;

/**
 * GuzzleClient test.
 *
 * @package Moments
 */
class GuzzleClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test constructor.
     */
    public function testConstructor()
    {
        $client = new GuzzleClient();

        $result = $client->getConfig('base_uri');
        $this->assertInstanceOf('GuzzleHttp\Psr7\Uri', $result);
        $this->assertEquals('http://gis.local.moments.xqtd.co', (string)$result);

        $result = $client->getConfig('headers');
        $this->assertEquals('application/json', $result['Accept']);
        $this->assertEquals('moments-php-sdk (https://github.com/xqtd/moments', $result['User-Agent']);
    }
}
