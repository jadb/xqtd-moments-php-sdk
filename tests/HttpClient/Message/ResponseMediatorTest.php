<?php
namespace Moments\Test\HttpClient\Message;

use GuzzleHttp\Psr7\Response;
use Moments\HttpClient\Message\ResponseMediator;

class ResponseMediatorTest extends \PHPUnit_Framework_TestCase
{
    public function testGetContent()
    {
        $mediator = new ResponseMediator(new Response(200, [], 'foo'));
        $result = $mediator->getContent();
        $expected = 'foo';
        $this->assertEquals($expected, $result);

        $mediator = new ResponseMediator(new Response(200, [], json_encode(['foo' => 'bar'])));
        $result = $mediator->getContent();
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $result);
    }

    public function testGetData()
    {
        $mediator = new ResponseMediator(new Response(200, [], 'foo'));
        $result = $mediator->getData();
        $expected = 'foo';
        $this->assertEquals($expected, $result);

        $mediator = new ResponseMediator(new Response(200, [], json_encode(['foo' => 'bar'])));
        $result = $mediator->getData();
        $expected = ['foo' => 'bar'];
        $this->assertEquals($expected, $result);

        $mediator = new ResponseMediator(new Response(200, [], json_encode(['foo' => 'bar', 'data' => ['bar' => 'foo']])));
        $result = $mediator->getData();
        $expected = ['bar' => 'foo'];
        $this->assertEquals($expected, $result);
    }
}
