<?php
namespace Moments\Test\Resource;

use GuzzleHttp\Psr7\Response;
use Moments\HttpClient\Message\ResponseMediator;
use Moments\Api\Stamps;

class StampsTest extends \PHPUnit_Framework_TestCase
{
    private $client;

    private $resource;

    public function setUp()
    {
        $this->client = $this->getMock('Moments\Client', ['get', 'delete']);
        $this->resource = new Stamps($this->client);
    }

    public function testGet()
    {
        $this->client->expects($this->once())
            ->method('get')
            ->with('stamps/foo', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->get('foo');
    }

    public function testDelete()
    {
        $this->client->expects($this->once())
            ->method('delete')
            ->with('stamps/foo', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->delete('foo');
    }
}
