<?php
namespace Moments\Test\Resource;

use GuzzleHttp\Psr7\Response;
use Moments\HttpClient\Message\ResponseMediator;
use Moments\Api\Markers;

class MarkersTest extends \PHPUnit_Framework_TestCase
{
    private $client;

    private $resource;

    public function setUp()
    {
        $this->client = $this->getMock('Moments\Client', ['get', 'multipartPost', 'delete']);
        $this->resource = new Markers($this->client);
    }

    public function testGetById()
    {
        $this->client->expects($this->once())
            ->method('get')
            ->with('markers/foo', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->get('foo');
    }

    public function testGetByQuery()
    {
        $this->client->expects($this->once())
            ->method('get')
            ->with('markers', ['q' => 'foo', 'page' => 1, 'limit' => 10, 'direction' => 'asc'])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->get(['q' => 'foo']);
    }

    public function testPost()
    {
        $this->client->expects($this->once())
            ->method('multipartPost')
            ->with('markers', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->post([]);
    }

    public function testPut()
    {
        $this->client->expects($this->once())
            ->method('multipartPost')
            ->with('markers/foo', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->put('foo', []);
    }

    public function testDelete()
    {
        $this->client->expects($this->once())
            ->method('delete')
            ->with('markers/foo', [])
            ->will($this->returnValue(new ResponseMediator(new Response())));

        $this->resource->delete('foo');
    }
}
