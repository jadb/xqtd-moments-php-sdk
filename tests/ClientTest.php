<?php
namespace Moments\Test;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Moments\Client;
use Moments\HttpClient\GuzzleClient;
use Moments\HttpClient\Message\ResponseMediator;

/**
 * Client test.
 *
 * @package Moments
 */
class ClientTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Moments\Client
     */
    private $client;

    /**
     * Setup.
     */
    public function setUp()
    {
        $this->client = new Client();
    }

    /**
     * Test resource.
     */
    public function testResource()
    {
        $this->assertInstanceOf('Moments\Api\Markers', $this->client->api('markers'));
    }

    /**
     * Test for undefined resource.
     *
     * @expectedException \Moments\Exception\UndefinedApiCallException
     * @expectedExceptionMessageRegExp /: "foo"/
     */
    public function testResourceThrowsExceptionOnUndefined()
    {
        $this->client->api('foo');
    }

    /**
     * Test magic method call.
     */
    public function testMagicMethodCall()
    {
        $this->assertInstanceOf('Moments\Api\Markers', $this->client->markers());
    }

    /**
     * Test magic method call for an undefined resource.
     *
     * @expectedException \Moments\Exception\BadMethodCallException
     * @expectedExceptionMessageRegExp /: "foo"/
     */
    public function testMagicMethodCallThrowsExceptionOnUndefined()
    {
        $this->client->foo();
    }

    /**
     * Test get.
     */
    public function testGet()
    {
        $response = new Response(200, [], json_encode(['success' => true, 'data' => ['id' => 'foo']]));

        $mock = new MockHandler([
            $response,
        ]);

        $handler = HandlerStack::create($mock);

        $this->client = new Client(new GuzzleClient(compact('handler')));

        $result = $this->client->get('foo/bar');
        $expected = new ResponseMediator($response);
        $this->assertEquals($expected, $result);
    }

    /**
     * Test post.
     */
    public function testPost()
    {
        $httpClient = $this->getMock('Moments\HttpClient\GuzzleClient', ['send']);
        $client = new Client($httpClient);

        $data = ['foo' => 'bar'];
        $request = new Request('post', 'foo');
        $response = new Response(200, [], json_encode(['success' => true, 'data' => ['id' => 'foo']]));
        $options = ['json' => $data, 'headers' => []];

        $httpClient->expects($this->once())
            ->method('send')
            ->with($request, $options)
            ->will($this->returnValue($response));

        $result = $client->post('foo', $data);
        $expected = new ResponseMediator($response);
        $this->assertEquals($expected, $result);
    }

    /**
     * Test multipart post.
     */
    public function testMultipartPost()
    {
        $httpClient = $this->getMock('Moments\HttpClient\GuzzleClient', ['send']);
        $client = $this->getMock('Moments\Client', ['getFileContents'], [$httpClient]);

        $response = new Response(200, [], json_encode(['success' => true, 'data' => ['id' => 'foo']]));

        $data = [
            'foo' => 'bar',
            'fam' => [
                'john' => 'doe',
                'doe' => 'jane',
            ],
            'tubes' => [
                'tmp_name' => [
                    'foo.bar',
                    'bar.foo',
                ],
            ]
        ];
        $request = new Request('post', 'foo');
        $options = [
            'headers' => [],
            'multipart' => [
                [
                    'name' => 'foo',
                    'contents' => 'bar',
                ],
                [
                    'name' => 'fam[john]',
                    'contents' => 'doe',
                ],
                [
                    'name' => 'fam[doe]',
                    'contents' => 'jane',
                ],
                [
                    'name' => 'tubes[0]',
                    'contents' => 'fileHandleMock0',
                ],
                [
                    'name' => 'tubes[1]',
                    'contents' => 'fileHandleMock1',
                ],
            ],
        ];

        $httpClient->expects($this->once())
            ->method('send')
            ->with($request, $options)
            ->will($this->returnValue($response));

        $client->expects($this->at(0))
            ->method('getFileContents')
            ->with('foo.bar')
            ->will($this->returnValue('fileHandleMock0'));

        $client->expects($this->at(1))
            ->method('getFileContents')
            ->with('bar.foo')
            ->will($this->returnValue('fileHandleMock1'));

        $result = $client->multipartPost('foo', $data);
        $expected = new ResponseMediator($response);
        $this->assertEquals($expected, $result);
    }
}
