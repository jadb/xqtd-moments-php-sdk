<?php
namespace Moments\Test\Exception;

use Moments\Exception\UndefinedApiCallException;

class UndefinedApiCallExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $result = (new UndefinedApiCallException('foo'))->getMessage();
        $expected = 'Undefined API requested: "foo"';
        $this->assertEquals($expected, $result);
    }
}
