<?php
namespace Moments\Test\Exception;

use Moments\Exception\InvalidArgumentException;

class InvalidArgumentExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $result = (new InvalidArgumentException('foo'))->getMessage();
        $expected = 'foo';
        $this->assertEquals($expected, $result);
    }
}
