<?php
namespace Moments\Test\Exception;

use Moments\Exception\BadMethodCallException;

class BadMethodCallExceptionTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructor()
    {
        $result = (new BadMethodCallException('foo'))->getMessage();
        $expected = 'Undefined method called: "foo"';
        $this->assertEquals($expected, $result);
    }
}
